%if %{?openEuler:1}0
%bcond_without graphics
%else
%bcond_with graphics
%endif

Name:           plymouth
Version:        22.02.122
Release:        2
License:        GPLv2+
Summary:        Graphical Boot Animation and Logger
URL:            https://www.freedesktop.org/wiki/Software/Plymouth
Source0:        https://gitlab.freedesktop.org/plymouth/plymouth/-/archive/%{version}/plymouth-%{version}.tar.gz

Patch0:         backport-drm-Retry-setting-scanout-buffer-on-failure.patch
Patch1:         backport-ply-utils-Reintroduce-ply_string_has_prefix-helper.patch
Patch2:         backport-ply-device-manager-Treat-SimpleDRM-drm-devices-as-fb.patch
Patch3:         backport-ply-device-manager-Move-verify_drm_device-higher-up-.patch
Patch4:         backport-ply-device-manager-Also-ignore-SimpleDRM-devs-in-col.patch

Patch9000:      carry-xattr-when-copying-files-to-initramfs.patch

BuildRequires:  gcc libtool systemd-devel kernel-headers
BuildRequires:  libxslt docbook-style-xsl gtk3-devel
BuildRequires:  gettext-devel intltool
%if %{with graphics}
BuildRequires:  libdrm-devel libpng-devel pango-devel cairo-devel
Requires:       system-logos
%endif
Requires:       findutils coreutils gzip cpio dracut

Provides:       %{name}-system-theme %{name}-scripts %{name}-plugin-two-step
Provides:       %{name}-graphics-libs %{name}-core-libs %{name}-plugin-fade-throbber
Provides:       %{name}-plugin-throbgress %{name}-plugin-space-flares
Provides:       %{name}-plugin-script %{name}-theme-script %{name}-plugin-label
Obsoletes:      %{name}-system-theme %{name}-scripts %{name}-plugin-two-step
Obsoletes:      %{name}-graphics-libs %{name}-core-libs %{name}-plugin-fade-throbber
Obsoletes:      %{name}-plugin-throbgress %{name}-plugin-space-flares
Obsoletes:      %{name}-plugin-script %{name}-theme-script %{name}-plugin-label

%description
Plymouth provides an attractive graphical boot animation in
place of the text messages that normally get shown.  Text
messages are instead redirected to a log file for viewing
after boot.

%package        devel
Summary:        Libraries and headers for writing Plymouth splash plugins
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description    devel
This package contains the libply and libplybootsplash libraries
and headers needed to develop 3rd party splash plugins for Plymouth.

%package        theme-fade-in
Summary:        Plymouth "Fade-In" theme
Requires:       %{name} = %{version}-%{release}

%description    theme-fade-in
This package contains the "Fade-In" boot splash theme for
Plymouth. It features a centered logo that fades in and out
while stars twinkle around the logo during system boot up.

%package        theme-glow
Summary:        Plymouth "Glow" theme
Requires:       %{name} = %{version}-%{release}

%description    theme-glow
This package contains the "Glow" boot splash theme for
Plymouth. It features a corporate-looking pie chart for
boot progress followed by a glowing logo once boot finishes.

%package        theme-solar
Summary:        Plymouth "Solar" theme
Requires:       %{name} = %{version}-%{release}

%description    theme-solar
This package contains the "Solar" boot splash theme for
Plymouth. It features a blue flamed sun with animated solar flares.

%package        theme-spinfinity
Summary:        Plymouth "Spinfinity" theme
Requires:       %{name} = %{version}-%{release}

%description    theme-spinfinity
This package contains the "Spinfinity" boot splash theme for
Plymouth. It features a centered logo and animated spinner that
spins in the shape of an infinity sign.

%package        theme-spinner
Summary:        Plymouth "Spinner" theme
Requires:       %{name} = %{version}-%{release}

%description    theme-spinner
This package contains the "spinner" boot splash theme for
Plymouth. It features a small spinner on a dark background.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1
autoreconf --install --symlink -Wno-portability

%build
%configure --enable-tracing                                      \
           --disable-tests                                       \
           --with-logo=%{_datadir}/pixmaps/system-logo-white.png \
           --with-background-start-color-stop=0x0073B3           \
           --with-background-end-color-stop=0x00457E             \
           --with-background-color=0x3391cd                      \
           --disable-gdm-transition                              \
           --enable-systemd-integration                          \
           --without-system-root-install                         \
%if %{without graphics}
           --disable-drm                                         \
           --disable-pango                                       \
           --without-udev                                        \
%endif
           --without-rhgb-compat-link

%make_build

%install
%make_install
%find_lang %{name}

%delete_la_and_a

%ldconfig_scriptlets

%postun theme-fade-in
export PLYMOUTH_PLUGIN_PATH=%{_libdir}/plymouth/
if [ $1 -eq 0 ]; then
    if [ "$(%{_sbindir}/plymouth-set-default-theme)" == "fade-in" ]; then
        %{_sbindir}/plymouth-set-default-theme --reset
    fi
fi

%postun theme-glow
export PLYMOUTH_PLUGIN_PATH=%{_libdir}/plymouth/
if [ $1 -eq 0 ]; then
    if [ "$(%{_sbindir}/plymouth-set-default-theme)" == "glow" ]; then
        %{_sbindir}/plymouth-set-default-theme --reset
    fi
fi

%postun theme-solar
export PLYMOUTH_PLUGIN_PATH=%{_libdir}/plymouth/
if [ $1 -eq 0 ]; then
    if [ "$(%{_sbindir}/plymouth-set-default-theme)" == "solar" ]; then
        %{_sbindir}/plymouth-set-default-theme --reset
    fi
fi

%postun theme-spinfinity
export PLYMOUTH_PLUGIN_PATH=%{_libdir}/plymouth/
if [ $1 -eq 0 ]; then
    if [ "$(%{_sbindir}/plymouth-set-default-theme)" == "spinfinity" ]; then
        %{_sbindir}/plymouth-set-default-theme --reset
    fi
fi

%postun theme-spinner
export PLYMOUTH_PLUGIN_PATH=%{_libdir}/plymouth/
# On upgrades replace charge with the new bgrt default
if [ $1 -eq 2 ]; then
    if [ "$(%{_sbindir}/plymouth-set-default-theme)" == "charge" ]; then
        %{_sbindir}/plymouth-set-default-theme bgrt
    fi
fi

%files -f %{name}.lang
%defattr(-,root,root)
%doc AUTHORS
%license COPYING
%dir %{_sysconfdir}/plymouth
%config(noreplace) %{_sysconfdir}/plymouth/plymouthd.conf
%if %{?openEuler:1}0
%dir %{_sysconfdir}/logrotate.d
%config(noreplace) %{_sysconfdir}/logrotate.d/bootlog
%else
%exclude %config(noreplace) %{_sysconfdir}/logrotate.d/bootlog
%endif

%{_bindir}/plymouth
%{_sbindir}/plymouthd
%{_sbindir}/plymouth-set-default-theme

%{_prefix}/lib/systemd/system/
%{_libdir}/libply.so.*
%{_libdir}/libply-splash-core.so.*
%{_libdir}/libply-boot-client.so.*
%{_libdir}/libply-splash-graphics.so.*
%dir %{_libdir}/plymouth
%{_libdir}/plymouth/*.so
%if %{with graphics}
%{_libdir}/plymouth/renderers/drm*
%endif
%{_libdir}/plymouth/renderers/frame-buffer*

%dir %{_libexecdir}/plymouth
%{_libexecdir}/plymouth/plymouth-update-initrd
%{_libexecdir}/plymouth/plymouth-generate-initrd
%{_libexecdir}/plymouth/plymouth-populate-initrd
%{_libexecdir}/plymouth/plymouthd-fd-escrow

%dir %{_datadir}/plymouth
%{_datadir}/plymouth/plymouthd.defaults
%{_datadir}/plymouth/themes/script
%{_datadir}/plymouth/themes/text/text.plymouth
%{_datadir}/plymouth/themes/tribar/tribar.plymouth
%{_datadir}/plymouth/themes/details/details.plymouth

%dir %{_localstatedir}/lib/plymouth
%{_localstatedir}/run/plymouth
%{_localstatedir}/spool/plymouth
%ghost %{_localstatedir}/lib/plymouth/boot-duration

%files devel
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_libdir}/plymouth/renderers/x11*
%{_includedir}/plymouth-1

%files theme-fade-in
%{_datadir}/plymouth/themes/fade-in

%files theme-glow
%{_datadir}/plymouth/themes/glow

%files theme-solar
%{_datadir}/plymouth/themes/solar

%files theme-spinfinity
%{_datadir}/plymouth/themes/spinfinity

%files theme-spinner
# bgrt is a variant of spinner with different settings in its .plymouth file
%{_datadir}/plymouth/themes/bgrt
%{_datadir}/plymouth/themes/spinner

%files help
%defattr(-,root,root)
%doc README
%{_mandir}/man?/*

%changelog
* Tue May 07 2024 yanglu <yanglu72@h-partners.com> - 22.02.122-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:add noreplace to /etc/logrotate.d/bootlog

* Wed Nov 02 2022 yanglu <yanglu72@h-partners.com> - 22.02.122-1
- Type:requirement
- CVE:NA
- SUG:NA
- DESC:update plymouth version to 22.02.122

* Fri Jun 17 2022 gaihuiying <eaglegai@163.com> - 0.9.5-4
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:add macros to control if need libdrm, pango, libpng and if need to exclude bootlog 
       
* Thu Dec 17 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.9.5-3
- carry xattr when copying files to initramfs

* Mon Sep 07 2020 lunankun <lunankun@huawei.com> - 0.9.5-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix source0 url

* Mon Aug 03 2020 yuboyun <yuboyun@huawei.com> - 0.9.5-1
- Type:NA
- Id:NA
- SUG:NA
- DESC:update to 0.9.5

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.9.4-2
- optimize spec

* Mon Jan 6 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.9.4-1
- update to 0.9.4

* Fri Dec 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.9.3-16
- description change

* Thu Sep 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.9.3-15
- Package init
